import { Db, ObjectId, Collection } from 'mongodb';
import { Graph } from './graph';

/**
 * 
 */
export class UserGraph extends Graph{

	private Users:Collection = null;

	//
	//
	constructor( db:Db ){
		super();
		this.Users = db.collection('users');
	}

	//
	//
	public getSchema():string {
		return `

			enum Gender {
				male
				female
				other
			}

			type User {
				_id: String
				firstname: String
				lastname: String
				gender: Gender
			}

			extend type Query {
				user(id: String): User
				users: [User]
			}

			extend type Mutation {
				createUser( firstname: String, lastname: String, gender: Gender): User
			}

		`;
	}

	//
	//
	public getQueryResolvers():any {
		return {
			users: async () => {
				return (await this.Users.find({}).toArray())
			},
	
			user: async (root:any, args:any) => {
				return await this.Users.findOne( new ObjectId(args.id));
			}	
		};
	}

	//
	//
	public getMutationResolvers():any {
		return {
			createUser: async (root:any, args:any) => {
				const res = await this.Users.insert(args);
				return await this.Users.findOne( new ObjectId( res.insertedId) );
			}			
		};
	}

}