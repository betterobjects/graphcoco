import { Db, ObjectId, Collection } from 'mongodb';
import { Graph } from './graph';

import * as _ from 'lodash';

// could use a proper pluralize library of course
const pluralize = (s:string):string => {
	return s + 's';
}

/**
 * Work in progress.. try to generalize the model behviour as much as possible
 * convention over configuration
 * we assume names, behaviour etc. as much as possible
 * this class is not yet used!
 */
export class ModelGraph extends Graph{

	private modelName:string;
	private collectionName:string;
	private entityName:string;
	private collection:Collection = null;

	//
	//
	constructor( 
			private db:Db,  
			private model:object ){
		super();
		this.modelName = _.first( Object.keys( model ) );
		this.entityName = _.lowerCase( this.modelName );
		this.collectionName = pluralize( this.entityName );
		this.collection = db.collection( this.collectionName );
	}



	public getSchema():string {
		return `

			enum Size {
				XL
				L
				S
				XS
			}

			type Item {
				_id: String
				number: String
				name: String
				description: String
				size: Size
			}

			extend type Query {
				item(id: String): Item
				items: [Item]
			}

			extend type Mutation {
				createItem( 
					number: String,
					name: String,
					description: String,
					size: Size
				): Item
			}

		`;
	}

}

var mg = new ModelGraph( null, { foo: {
	id: 'string',
	name: 'string',
	description: 'string', 
	size: 'number' 
}});