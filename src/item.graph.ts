import { Db, ObjectId, Collection } from 'mongodb';
import { Graph } from './graph';

/**
 * 
 */
export class ItemGraph extends Graph{

	private collection:Collection = null;

	//
	//
	constructor( db:Db ){
		super();
		this.collection = db.collection('items');
	}

	//
	//
	public getSchema():string {
		return `

			enum Size {
				XL
				L
				S
				XS
			}

			type Item {
				_id: String
				number: String
				name: String
				description: String
				size: Size
			}

			extend type Query {
				item(id: String): Item
				items: [Item]
			}

			extend type Mutation {
				createItem( 
					number: String,
					name: String,
					description: String,
					size: Size
				): Item
			}

		`;
	}

	//
	//
	public getQueryResolvers():any {
		return {
			items: async () => {
				return (await this.collection.find({}).toArray())
			},
	
			item: async (root:any, args:any) => {
				return await this.collection.findOne( new ObjectId(args.id));
			}	
		};
	}

	//
	//
	public getMutationResolvers():any {
		return {
			createItem: async (root:any, args:any) => {
				const res = await this.collection.insert(args);
				return await this.collection.findOne( new ObjectId( res.insertedId) );
			}			
		};
	}

}