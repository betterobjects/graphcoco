import { Db, ObjectId, Collection } from 'mongodb';
import { Graph } from './graph';

/**
 * 
 */
export class BlogGraph extends Graph{

	private Posts:Collection = null;
	private Comments:Collection = null;

	//
	//
	constructor( db:Db ){
		super();
		this.Posts = db.collection('posts');
		this.Comments = db.collection('comments');
	}

	//
	//
	public getSchema():string {
		return `
			type Post {
				_id: String
				title: String
				content: String
				comments: [Comment]
			}
		
			type Comment {
				_id: String
				postId: String
				content: String
				post: Post
			}
		
			extend type Query {			
				post(id: String): Post
				posts: [Post]
				comment(id: String): Comment
			}

			extend type Mutation {			
				createPost(title: String, content: String): Post
				createComment(postId: String, content: String): Comment
			}
		`;
	}

	//
	//
	public getQueryResolvers():any {
		return {
			post: async (root:any, args:any) => {
				return await this.Posts.findOne(new ObjectId(args.id));
			},
	
			posts: async () => {
				return (await this.Posts.find({}).toArray());
			},
	
			comment: async (root:any, args:any) => {
				return await this.Comments.findOne(new ObjectId(args.id));
			}
		};
	}

	//
	//
	public getTypeResolvers():any {
		return {
			Post: {
				comments: async (id:string) => {
					return await this.Comments.find({postId: id}).toArray();
				}
			},

			Comment: {
				post: async (postId:string) => {
					return await this.Posts.findOne(new ObjectId(postId));
				}
			}
		};
	}

	//
	//
	public getMutationResolvers():any {
		return {
			createPost: async (root:any, args:any, context:any, info:any) => {
				const res = await this.Posts.insert(args);			
				return  await this.Posts.findOne( new ObjectId(res.insertedId ) );
			},

			createComment: async (root:any, args:any) => {
				const res = await this.Comments.insert(args);
				return await this.Comments.findOne( new ObjectId( res.insertedId ) );
			}
		};
	}

}