import { MongoClient } from 'mongodb'
import { graphqlExpress, graphiqlExpress } from 'graphql-server-express';
import { makeExecutableSchema } from 'graphql-tools'
import * as express from 'express';
import * as bodyParser from 'body-parser'
import * as cors from 'cors';

import { Graph } from './graph';
import { UserGraph } from './user.graph';
import { BlogGraph } from './blog.graph';
import { ItemGraph } from './item.graph';

const MONGO_URL = 'mongodb://localhost:27017/blog';
const URL = 'http://localhost';
const PORT = 3001;


//
//
const start = async () => {

	const db = await MongoClient.connect(MONGO_URL);

	var graphs:[Graph] = [ 
		new UserGraph(db),
		new BlogGraph( db ),
		new ItemGraph( db )
	];	

	const executableSchema = makeExecutableSchema( Graph.createSchema( graphs ) );

	const app = express();

	app.use(cors());

	app.use('/graphql', bodyParser.json(), graphqlExpress( {schema: executableSchema}));

	app.use('/graphiql', graphiqlExpress({
		endpointURL: '/graphql'
	}));

	app.listen(PORT, () => {
		console.log(`Visit ${URL}:${PORT}`)
	});
}

start();
