import * as _ from 'lodash';

export abstract class Graph {

	//
	//
	abstract getSchema():string;
	
	//
	//
	public getQueryResolvers():any { return {}; }

	//
	//
	public getMutationResolvers():any { return {}; }

	//
	//
	public getTypeResolvers():any { return {}; }

	private static schemaDefinfition = `		
		type Query { ping: Boolean }
		type Mutation { ping( bar:Boolean ): Boolean }

		schema {
			query: Query 
			mutation: Mutation
		}
	`;

	private static emptyResolvers:any = { 
		Query: [],
		Mutation: [] 
	};
	
	//
	//
	static createSchema( graphDefinitions:[Graph]):any {
		
		var resolvers:any = _.cloneDeep( Graph.emptyResolvers );
		graphDefinitions.forEach( g => {
			_.merge( resolvers['Query'], g.getQueryResolvers() );
			_.merge( resolvers['Mutation'], g.getMutationResolvers() );
			_.merge( resolvers, g.getTypeResolvers() );
		});
		
		return {
			typeDefs: [			
				Graph.schemaDefinfition,
				...graphDefinitions.map( g => g.getSchema() )
			],
			resolvers: resolvers
		};
	}

}

