# graphcoco
experimantal project with graphql/mongodb/typescript and a convention over configuration approach

You need mongodb with a DB blog running (mongodb://localhost:27017/blog)

`npm run start` will compile and run the express server at the printed endpoint

Open http://localhost:3001/graphiql and start playing around with the models.
